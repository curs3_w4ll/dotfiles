# Dotfiles

Hey you, what's up?

If you are here, you probably want to check my configuration for one of the tools I use (zsh, neovim...).  
Here are some things you should know before diving in.

First, you should know this repository comes from [Github](https://github.com/Curs3W4ll/Dotfiles), if you are already on Github jump to the next paragraph. 
The GitHub repository provides more features and links (wiki...) so it is recommended to see [this repository on Github](https://github.com/Curs3W4ll/Dotfiles).

I manage my dotfiles using [chezmoi](https://www.chezmoi.io/).  
`chezmoi` is a Dotfiles manager CLI with many cool features, it can handle files, templates, secrets and more.

<details>
<summary>More information and tips for chezmoi</summary>

### How does `chezmoi` works?

`chezmoi` is just an add-on script on top of a Git repository.  
It will be linked to a Git repository and will sync your files and more using this repository.

Using `chezmoi`, you can't select the files you want to sync, but you can do this using my [custom Scripts](todo) <!-- TODO: Add correct link -->

### Install `chezmoi`

```sh
sh -c "$(curl -fsLS get.chezmoi.io)"
```

See [official installation methods](https://www.chezmoi.io/install/).

### Use these dotfiles on a new machine

```sh
chezmoi init --apply Curs3W4ll
```

As `Curs3W4ll` is my username, so `chezmoi` will retrieve my `Dotfiles` repository on my account.

### Sync to the latest dotfiles version

```sh
chezmoi update
```

### See changes with the latest dotfiles version without applying any change

```sh
chezmoi git pull -- --autostash --rebase && chezmoi diff
```

And you can apply these changes using

```sh
chezmoi apply
```

</details>

## What's in this?

In this Dotfiles repository, you will find configurations I use, for instance:
- Neovim configuration ([vimscript](todo) (old, not maintained) and [lua](todo)) <!-- TODO: Add link to file/folder -->
- [Terminator configuration](todo) <!-- TODO: Add link to file/folder -->
- [Zsh configuration](todo) <!-- TODO: Add link to file/folder -->

You will also find some documentation about common topics [here](todo). <!-- TODO: Add correct link -->

### Neovim

#### LSPs

#### DAPs

#### Linters

Some linters need to be installed in a specific way or need some dependencies, so here is a quick guide to see what you need to do.

<details>
<summary>Vale</summary>

#### Installation

`Vale` is not compatible with node > v16 so make sure Neovim uses a node version < v16.  
Use the following command to see which version of node Neovim is using.

```
:checkhealth mason
```
`mason.nvim [Languages]` > `node`.

Use [n](https://www.npmjs.com/package/n) to easily switch between nodeJS versions.

#### Configuration

https://vale.sh/docs/topics/config/

</details>

#### Formatters
