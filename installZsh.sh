#!/bin/bash

NoColor="\033[0m"
CyanColor="\033[0;36m"
RedColor="\033[0;91m"

dotfilesPath="/tmp/CDotfiles"
destPath="$HOME"

function confirm {
    local _response
    while true; do
        if [ -n "$1" ]; then
            echo -n $1
        else
            echo -n "Are you sure"
        fi
        echo -n " [y/n] ? "
        read -r _response
        case "$_response" in
            [Yy][Ee][Ss]|[Yy]|"")
                return 0
            ;;
            [Nn][Oo]|[Nn])
                return 1
            ;;
            *)
                echo "Invalid input, Please response Yes or No"
            ;;
        esac
    done
}

set +eo pipefail
batBinPath=$( which bat )
if [ ! $? -eq 0 ]; then
    echo -e "\n${RedColor}Please install bat first${NoColor}"
    exit 1
else
    echo -e "${CyanColor}Using bat at $batBinPath${NoColor}"
fi
if [[ ! -d $HOME/.oh-my-zsh ]]; then
    echo -e "${RedColor}Please install oh my zsh first (run: sh -c \"\$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)\")${NoColor}"
    exit 1
else
    echo -e "${CyanColor}Using ohmyzsh at ${ZSH}${NoColor}"
fi
set -eo pipefail

confirm "Using this script will remove the existing zsh configuration ('.zshrc' file, $HOME/.oh-my-zsh dir), Continue"

echo -e "${CyanColor}Cloning Dotfiles to ${dotfilesPath}${NoColor}"
rm -rf $dotfilesPath
git clone https://github.com/Curs3W4ll/Dotfiles ${dotfilesPath}

echo -e "${CyanColor}Copying zsh configuration${NoColor}"
rm -rf $destPath/.zshrc
cp $dotfilesPath/data/zsh/zshrc $destPath/.zshrc

zshCustomPath=${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}

zshInstallPath=$zshCustomPath/plugins/k
if [[ ! -d $zshInstallPath ]]; then
    echo -e "${CyanColor}Installing k zsh plugin${NoColor}"
    git clone https://github.com/supercrabtree/k $zshInstallPath
fi

zshInstallPath=$zshCustomPath/plugins/zsh-autosuggestions
if [[ ! -d $zshInstallPath ]]; then
    echo -e "${CyanColor}Installing zsh-autosuggestions plugin${NoColor}"
    git clone https://github.com/zsh-users/zsh-autosuggestions $zshInstallPath
fi

zshInstallPath=$zshCustomPath/plugins/zsh-syntax-highlighting
if [[ ! -d $zshInstallPath ]]; then
    echo -e "${CyanColor}Installing zsh-syntax-highlighting plugin${NoColor}"
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $zshInstallPath
fi

zshInstallPath=$zshCustomPath/themes/powerlevel10k
if [[ ! -d $zshInstallPath ]]; then
    echo -e "${CyanColor}Installing PowerLevel10k theme${NoColor}"
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git $zshInstallPath
fi

echo -e "${CyanColor}Removing cloned repository${NoColor}"
rm -rf $dotfilesPath

echo -e "${CyanColor}When p10k configuration is done, uncomment the following prompt elements if you want:${NoColor}"
echo -e "${CyanColor}  - status${NoColor}"
echo -e "${CyanColor}  - command_execution_time${NoColor}"
echo -e "${CyanColor}  - background_jobs${NoColor}"
echo -e "${CyanColor}  - direnv${NoColor}"
echo -e "${CyanColor}  - asdf${NoColor}"
echo -e "${CyanColor}  - virtualenv${NoColor}"
echo -e "${CyanColor}  - anaconda${NoColor}"
echo -e "${CyanColor}  - pyenv${NoColor}"
echo -e "${CyanColor}  - goenv${NoColor}"
echo -e "${CyanColor}  - nodenv${NoColor}"
echo -e "${CyanColor}  - nvm${NoColor}"
echo -e "${CyanColor}  - nodeenv${NoColor}"
echo -e "${CyanColor}  - node_version${NoColor}"
echo -e "${CyanColor}  - go_version${NoColor}"
echo -e "${CyanColor}  - rust_version${NoColor}"
echo -e "${CyanColor}  - dotnet_version${NoColor}"
echo -e "${CyanColor}  - php_version${NoColor}"
echo -e "${CyanColor}  - laravel_version${NoColor}"
echo -e "${CyanColor}  - java_version${NoColor}"
echo -e "${CyanColor}  - package${NoColor}"
echo -e "${CyanColor}  - rbenv${NoColor}"
echo -e "${CyanColor}  - rvm${NoColor}"
echo -e "${CyanColor}  - fvm${NoColor}"
echo -e "${CyanColor}  - luaenv${NoColor}"
echo -e "${CyanColor}  - jenv${NoColor}"
echo -e "${CyanColor}  - plenv${NoColor}"
echo -e "${CyanColor}  - perlbrew${NoColor}"
echo -e "${CyanColor}  - phpenv${NoColor}"
echo -e "${CyanColor}  - scalaenv${NoColor}"
echo -e "${CyanColor}  - haskell_stack${NoColor}"
echo -e "${CyanColor}  - kubecontext${NoColor}"
echo -e "${CyanColor}  - terraform${NoColor}"
echo -e "${CyanColor}  - terraform_version${NoColor}"
echo -e "${CyanColor}  - aws${NoColor}"
echo -e "${CyanColor}  - aws_eb_env${NoColor}"
echo -e "${CyanColor}  - azure${NoColor}"
echo -e "${CyanColor}  - gcloud${NoColor}"
echo -e "${CyanColor}  - google_app_cred${NoColor}"
echo -e "${CyanColor}  - toolbox${NoColor}"
echo -e "${CyanColor}  - context${NoColor}"
echo -e "${CyanColor}  - nordvpn${NoColor}"
echo -e "${CyanColor}  - ranger${NoColor}"
echo -e "${CyanColor}  - nnn${NoColor}"
echo -e "${CyanColor}  - xplr${NoColor}"
echo -e "${CyanColor}  - vim_shell${NoColor}"
echo -e "${CyanColor}  - midnight_commander${NoColor}"
echo -e "${CyanColor}  - nix_shell${NoColor}"
echo -e "${CyanColor}  - load${NoColor}"
echo -e "${CyanColor}  - disk_usage${NoColor}"
echo -e "${CyanColor}  - ram${NoColor}"
echo -e "${CyanColor}  - todo${NoColor}"
echo -e "${CyanColor}  - timewarrior${NoColor}"
echo -e "${CyanColor}  - taskwarrior${NoColor}"
echo -e "${CyanColor}  - time${NoColor}"
echo -e "${CyanColor}  - newline${NoColor}"
echo -e "${CyanColor}  - ip${NoColor}"
echo -e "${CyanColor}  - public_ip${NoColor}"
echo -e "${CyanColor}  - battery${NoColor}"
echo -e "${CyanColor}  - wifi${NoColor}"
echo ""
echo -e "${CyanColor}INSTALLATION SUCCESSFULL${NoColor}"
echo -e "${CyanColor}Please restart your shell to configure the new one${NoColor}"
