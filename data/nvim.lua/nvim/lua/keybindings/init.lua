vim.g.mapleader = ","

require("keybindings.bindings")
require("keybindings.todo-comments")
require("keybindings.persistence")
require("keybindings.gitsigns")
require("keybindings.neoconf")
require("keybindings.lspconfig")
require("keybindings.hlslens")
require("keybindings.formatter")
require("keybindings.dap")
require("keybindings.neo-tree")
require("keybindings.bufferline")
require("keybindings.comment")
